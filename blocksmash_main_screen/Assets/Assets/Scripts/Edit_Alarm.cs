﻿using UnityEngine;
using System.Collections;

public class Edit_Alarm : MonoBehaviour {

	public int time;
	public int tmpalarm;
	private int digit1,digit2,digit3,digit4;
	public GameObject wheel1,wheel2,wheel3,wheel4;
	
	void Start () {
		//lock screen rotation
		Screen.orientation = ScreenOrientation.Portrait;

		//scale maincamera
		Camera.main.orthographicSize = Screen.height/2;

		//lock screen rotation
		Screen.orientation = ScreenOrientation.Portrait;
		
		System.DateTime timenow = System.DateTime.Now;
		time = timenow.Hour * 100 + timenow.Minute;
		setinitialtime (time);
		tmpalarm = PlayerPrefs.GetInt ("PlayerPrefs_tmpalarm");//temporary
	}

	void Update () {

		// first
		if(Input.GetKeyDown("1")){
			wheel1.transform.Rotate(120,0,0);//360/3
			digit1++;
		}
		else if(Input.GetKeyDown("q")){
			wheel1.transform.Rotate(-120,0,0);//360/3
			digit1--;
			if(digit1<=-1){
				digit1=2;
			}
		}

		// second
		if(Input.GetKeyDown("2")){
			wheel2.transform.Rotate(36,0,0);//360/10
			digit2++;
			if(digit2>=10){
				wheel1.transform.Rotate (120, 0, 0);
				digit1++;
			}
		}
		else if(Input.GetKeyDown("w")){
			wheel2.transform.Rotate(-36,0,0);//360/10
			digit2--;
			if(digit2<=-1){
				digit2=9;
			}
		}

		// third
		if(Input.GetKeyDown("3")){
			wheel3.transform.Rotate(60,0,0);//360/6
			digit3++;
			if(digit3>=6){
				wheel2.transform.Rotate (36, 0, 0);
				digit2++;
				if(digit2>=10){
					wheel1.transform.Rotate (120, 0, 0);
					digit1++;
				}
			}
		}
		else if(Input.GetKeyDown("e")){
			wheel3.transform.Rotate(-60,0,0);//360/6
			digit3--;
			if(digit3<=-1){
				digit3=5;
			}
		}

		// fourth
		if(Input.GetKeyDown("4")){
			wheel4.transform.Rotate(36,0,0);//360/10
			digit4++;
			if(digit4>=10){
				wheel3.transform.Rotate (60, 0, 0);
				digit3++;
				if(digit3>=6){
					wheel2.transform.Rotate (36, 0, 0);
					digit2++;
					if(digit2>=10){
						wheel1.transform.Rotate (120, 0, 0);
						digit1++;
					}
				}
			}
		}
		else if(Input.GetKeyDown("r")){
			wheel4.transform.Rotate(-36,0,0);//360/10
			digit4--;
			if(digit4<=-1){
				digit4=9;
			}
		}
	
		digit1%=3;
		digit2%=10;
		digit3 %= 6;
		digit4 %= 10;

		time = digit1 * 1000 + digit2 * 100 + digit3 * 10 + digit4;
		tmpalarm = time;//temporary
		PlayerPrefs.SetInt ("PlayerPrefs_tmpalarm",tmpalarm);//temporary

		// Press L to go to main level
		if(Input.GetKeyDown("l")){
			Application.LoadLevel("main");
		}
	}

	void setinitialtime(int time){

		digit1 = time / 1000;
		digit2 = time % 1000 / 100;
		digit3 = time % 100 / 10;
		digit4 = time % 10;

		time = digit1 * 1000 + digit2 * 100 + digit3 * 10 + digit4;

		wheel1.transform.Rotate (digit1*120, 0, 0);
		wheel2.transform.Rotate (digit2*36, 0, 0);
		wheel3.transform.Rotate (digit3*60, 0, 0);
		wheel4.transform.Rotate (digit4*36, 0, 0);

	}
}
